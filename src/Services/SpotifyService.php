<?php

declare(strict_types=1);

namespace App\Services;

use App\Exception\InvalidStateException;
use SpotifyWebAPI\Session;
use SpotifyWebAPI\SpotifyWebAPI;

class SpotifyService
{
    private SpotifyWebAPI $api;
    private Session $session;
    private string $state;
    private string $accessToken;

    private const AUTHORIZATION_SCOPES = [
        'playlist-read-private',
        'playlist-modify-private',
        'user-library-modify'
    ];

    public function __construct()
    {
        if (empty($this->accessToken)) {
            $this->session = new Session(
                $_ENV['SPOTIFY_CLIENT_ID'],
                $_ENV['SPOTIFY_CLIENT_SECRET'],
                $_ENV['SPOTIFY_REDIRECT_URI'],
            );

            $this->state = $this->session->generateState();
            $options = [
                'scope' => self::AUTHORIZATION_SCOPES,
                'state' => $this->state,
            ];

            echo $this->session->getAuthorizeUrl($options) . PHP_EOL;
            $this->api = new SpotifyWebAPI(['return_assoc' => true]);
        }
    }

    /**
     * @throws InvalidStateException
     */
    public function setSpotifyAuthorizationCode(string $callbackUri): void
    {
        $callbackUriQuery = parse_url($callbackUri, PHP_URL_QUERY);
        parse_str($callbackUriQuery, $params);
        if ($params['state'] !== $this->state) {
            throw new InvalidStateException();
        }
        $this->session->requestAccessToken($params['code']);
        $this->accessToken = $this->session->getAccessToken();
    }

    public function search(array $songs): array
    {
        $this->api->setAccessToken($this->accessToken);
        $tracks = [];
        $songsFilesCount = count($songs);
        $tracksFound = 0;
        echo sprintf('Found %s files to search tracks for.' . PHP_EOL, $songsFilesCount);

        foreach ($songs as $key => $songName) {
            try {
                $searchResults = $this->api->search($songName, 'track', ['market' => 'RO']);
                if (!empty($trackId = $searchResults['tracks']['items'][0]['id'])) {
                    $tracks[$key] = [
                        'id' => $trackId,
                        'name' => $songName,
                        'added' => false,
                    ];
                    $tracksFound++;
                    echo sprintf(
                        '[%s/%s] Found a result for %s.' . PHP_EOL,
                        $tracksFound,
                        $songsFilesCount,
                        $songName,
                    );
                } else {
                    echo sprintf('Did not found a result for %s.' . PHP_EOL, $songName);
                }
            } catch (\Throwable $t) {
                echo sprintf(
                    'Search error for song with name %s. Error: %s.' . PHP_EOL,
                    $songName,
                    $t->getMessage()
                );
            }
        }

        return $tracks;
    }

    public function addToLikedSongs(array $tracks): array
    {
        $this->api->setAccessToken($this->accessToken);
        $addedCount = 0;
        $tracksTotal = count($tracks);
        foreach ($tracks as $key => $trackData) {
            if ($trackData['added'] === false) {
                try {
                    $added = $this->api->addMyTracks($trackData['id']);
                    $addedCount += $added ? 1 : -1;
                    echo sprintf(
                        '[%s/%s] Added %s to the playlist.' . PHP_EOL,
                        $addedCount,
                        $tracksTotal,
                        $trackData['name'],
                    );
                } catch (\Throwable $t) {
                    $added = false;
                    echo sprintf(
                        'Error adding track with id %s to the playlist. Error: %s.' . PHP_EOL,
                        $trackData['id'],
                        $t->getMessage()
                    );
                }
                $tracks[$key]['added'] = $added;
            }
        }
        echo sprintf('Added %s tracks to the playlist.', $addedCount);

        return $tracks;
    }

    public function copyTracksFromPlaylistToPlaylist(string $playlistFrom, ?string $playlistTo): void
    {
        $this->api->setAccessToken($this->accessToken);

        $items = [];
        $this->getItemsFromPlaylistRecursive($playlistFrom, items: $items);
        $items = array_merge(...$items);
        $tracks = array_map(static function ($item) {
            return $item['track']['id'];
        }, $items);
        $tracksCount = count($tracks);
        echo sprintf('Found %s tracks in the playlist %s.' . PHP_EOL, $tracksCount, $playlistFrom);

        foreach (array_chunk($tracks, 50) as $tracksChunk) {
            echo sprintf(
                'Copying %s tracks to the playlist %s.' . PHP_EOL,
                count($tracksChunk),
                $playlistTo ?? 'Your Liked'
            );
            if (!empty($playlistTo)) {
                $this->api->addPlaylistTracks($playlistTo, $tracksChunk);
            } else {
                $this->api->addMyTracks($tracksChunk);
            }
        }
    }

    private function getItemsFromPlaylistRecursive(string $playlistId, array &$items, array $options = []): array
    {
        $playlistTracksResponse = $this->api->getPlaylistTracks($playlistId, $options);
        $items[] = $playlistTracksResponse['items'];
        if ($playlistTracksResponse['next'] !== null) {
            $items = $this->getItemsFromPlaylistRecursive(
                $playlistId,
                $items,
                ['offset' => $playlistTracksResponse['offset'] + 100],
            );
        }

        return $items;
    }
}