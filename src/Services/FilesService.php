<?php

declare(strict_types=1);

namespace App\Services;

class FilesService
{
    private const FILES_CLEANUP_REGEX_PATTERN =
        '/[^A-Za-z\-\s]|(mp3)|(\d*k)|(official video)|(lyric video)|(music video)|(official)/';

    public function getListOfMp3Files(string $path, bool $includingSubdirectories): array
    {
        $files = [];
        if ($includingSubdirectories) {
            $this->computeListOfFilesRecursive($path, $files);
        } else {
            $files = scandir($path);
        }

        $mp3Files = [];

        foreach ($files as $file) {
            $file = strtolower($file);
            if (str_contains($file, '.mp3')) {
                $mp3Files[] = $this->cleanFileName($file, $includingSubdirectories);
            }
        }

        return $mp3Files;
    }

    private function cleanFileName(string $fileName, bool $hasFullPath): string
    {
        if ($hasFullPath) {
            $fileName = substr($fileName, strrpos($fileName, '\\'));
        }
        return trim(preg_replace(self::FILES_CLEANUP_REGEX_PATTERN, '', $fileName));
    }

    private function computeListOfFilesRecursive(string $path, array &$files): void
    {
        $filesInPath = array_diff(scandir($path), ['.', '..']);

        foreach ($filesInPath as $item) {
            $item = $path . DIRECTORY_SEPARATOR . $item;
            if (is_dir($item)) {
                $this->computeListOfFilesRecursive($item . DIRECTORY_SEPARATOR, $files);
            } else {
                $files[] = $item;
            }
        }

    }
}