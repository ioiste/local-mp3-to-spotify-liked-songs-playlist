<?php

declare(strict_types=1);

namespace App\Command;

use App\Services\SpotifyService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Throwable;

class SpotifyPlaylistToPlaylist extends Command
{
    public const NAME = 'spotify-playlist-to-playlist';

    public function __construct(
        private readonly SpotifyService $spotify,
    )
    {
        parent::__construct(self::NAME);
    }

    protected function configure(): void
    {
        $this->setName(self::NAME);
        $this->setDescription('Copy songs from a Spotify playlist to another.');
        $this->addOption(
            'playlistFrom',
            'pf',
            InputOption::VALUE_REQUIRED,
            'Playlist ID from which to copy.',
        );
        $this->addOption(
            'playlistTo',
            'pt',
            InputOption::VALUE_OPTIONAL,
            'Playlist ID to which to copy. Defaults to Your Likes playlist.',
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $helper = $this->getHelper('question');
            $callbackUriQuestion = new Question('Spotify callback URI: ', '');
            $callbackUri = $helper->ask($input, $output, $callbackUriQuestion);
            $this->spotify->setSpotifyAuthorizationCode($callbackUri);

            $playlistFrom = $input->getOption('playlistFrom');
            $playlistTo = $input->getOption('playlistTo');

            $this->spotify->copyTracksFromPlaylistToPlaylist($playlistFrom, $playlistTo);
        } catch (Throwable $t) {
            echo sprintf('Error while executing command %s: %s', self::NAME, $t->getMessage());
            return self::FAILURE;
        }

        return self::SUCCESS;
    }
}