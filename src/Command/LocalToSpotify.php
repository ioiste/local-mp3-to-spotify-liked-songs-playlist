<?php

declare(strict_types=1);

namespace App\Command;

use App\Services\FilesService;
use App\Services\SpotifyService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Throwable;

class LocalToSpotify extends Command
{
    private const TRACKS_JSON_FILE_PATH = __DIR__ . '/../../%s-tracks.json';

    public const NAME = 'local-to-spotify';

    public function __construct(
        private readonly SpotifyService $spotify,
        private readonly FilesService $filesService,
    )
    {
        parent::__construct(self::NAME);
    }

    protected function configure(): void
    {
        $this->setName(self::NAME);
        $this->setDescription('Transfer songs from local file system to Spotify.');
        $this->addOption(
            'path',
            'p',
            InputOption::VALUE_REQUIRED,
            'The path to the files.',
        );
        $this->addOption(
            'cache',
            'c',
            InputOption::VALUE_OPTIONAL,
            'Use cached data? Defaults to true.',
            true
        );
        $this->addOption(
            'recursive',
            'r',
            InputOption::VALUE_OPTIONAL,
            'Scan subdirectories in path? Defaults to false.',
            false
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $helper = $this->getHelper('question');
            $callbackUriQuestion = new Question('Spotify callback URI: ', '');
            $callbackUri = $helper->ask($input, $output, $callbackUriQuestion);
            $this->spotify->setSpotifyAuthorizationCode($callbackUri);

            $path = $input->getOption('path');
            $recursive = in_array($input->getOption('recursive'), ['true', true], true);
            $useCache = in_array($input->getOption('cache'), ['true', true], true);

            $files = $this->filesService->getListOfMp3Files($path, $recursive);

            echo sprintf('Found %s files in %s.' . PHP_EOL, count($files), $path);
            $tracksFileName = sprintf(self::TRACKS_JSON_FILE_PATH, md5($path));

            if ($useCache) {
                echo sprintf('Using cache from file %s.' . PHP_EOL, $tracksFileName);
                try {
                    $json = file_get_contents($tracksFileName);
                    $tracks = json_decode($json ?? '{}', true);
                } catch (Throwable $t) {
                    echo sprintf('Could not read tracks from cache: %s.' . PHP_EOL, $t->getMessage());
                    $tracks = [];
                }
            }

            if (empty($tracks)) {
                echo 'Not using cache or cache was not available.' . PHP_EOL;
                $tracks = $this->spotify->search($files);
                file_put_contents($tracksFileName, json_encode($tracks));
            }
            echo sprintf('Adding %s tracks to Your Liked Spotify playlist.' . PHP_EOL, count($tracks));

            $tracksAdded = $this->spotify->addToLikedSongs($tracks);
            file_put_contents($tracksFileName, json_encode($tracksAdded));

        } catch (Throwable $t) {
            echo sprintf('Error while executing command %s: %s', self::NAME, $t->getMessage());
            return self::FAILURE;
        }
        return self::SUCCESS;
    }
}