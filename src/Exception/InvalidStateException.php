<?php

namespace App\Exception;

class InvalidStateException extends \Exception
{
    protected $message =  'Received an invalid state from the callback URI.';
}