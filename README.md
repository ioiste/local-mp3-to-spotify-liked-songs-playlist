## Spotify tools
This requires PHP 8.1 or later and Composer installed.

1. Run `composer install`
2. Make a copy of `.env.example` to a `.env` file and fill in your Spotify app credentials

#### Transfer local MP3s files to Spotify Liked Songs Playlist
1. Run `php bin/console local-to-spotify --path='H:\Path\To\MP3s'`
   * The command creates .json files to keep track of what it previously did. You can disable that by passing the `--cache=false` option to the command
   * The command does not scan subdirectories in the provided path. You can enable that with the `--recursive=true` option
2. You'll see a URL in the command output. Open that URL in the browser.
3. When asked, paste the callback URI which you were redirected to in the browser
4. 

#### Transfer tracks from one playlist to another
1. Run `php bin/console spotify-playlist-to-playlist --playlistFrom=SpotifyPlaylistId`
    * Specify the target playlist with the `--playlistTo=SpotifyPlaylistId` option. If not specified, the target playlist is the user's 'Liked Songs' playlist
2. You'll see a URL in the command output. Open that URL in the browser.
3. When asked, paste the callback URI which you were redirected to in the browser
